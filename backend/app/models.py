from datetime import datetime
import numpy as np
import json
from app import db


class Game(db.Model):
    __tablename__ = 'games'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    last_save_at = db.Column(db.DateTime, default=datetime.utcnow)
    table_size = db.Column(db.Integer, default=4, nullable=False)
    table = db.Column(db.String(512))

    def to_dict(self):
        return {
            'id': self.id,
            'created_at': self.created_at,
            'last_save_at': self.last_save_at,
            'table_size': self.table_size,
            'table': self.table
        }

    def initialize_random_table(self):
        size = self.table_size
        table = np.random.choice(
            size * size, (size, size),
            replace=False).tolist()
        for row in table:
            for col, i in enumerate(row):
                if i == 0:
                    row[col] = None

        self.table = json.dumps(table)

    def initialize_classic_table(self):
        size = self.table_size
        table = []
        for i in range(0, size - 1):
            table.append(list(range(i * size + 1, i * size + size + 1)))
        table.append(list(range(size * size - size + 1, size * size)))
        table[-1].append(None)

        self.table = json.dumps(table)
