import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Http, Response } from '@angular/http';
import { Game } from './game';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const API_URL = environment.apiUrl;

@Injectable()
export class ApiService {

  constructor(
    private http: Http
  ) {
  }

  public newGameClassic() {
    return this.http
    .post(API_URL + '/game/classic', "")
    .map(response => {
      return new Game(response.json());
    })
    .catch(this.handleError);
  }

  public newGameRandom() {
    return this.http
    .post(API_URL + '/game/random', "")
    .map(response => {
      return new Game(response.json());
    })
    .catch(this.handleError);
  }

  public getGameByID(game_id: number) {
    return this.http
      .get(API_URL + '/game?id=' + game_id)
      .map(response => {
        return new Game(response.json());
      })
      .catch(this.handleError);
  }

  public updateGame(col: number, row: number) {
    return this.http
    .post(API_URL + '/game', "col=" + col + "&row=" + row)
    .map(response => {
      return new Game(response.json());
    })
    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
